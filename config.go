package main

import "github.com/spf13/viper"

type Config struct {
	url      string
	user     string
	password string
}

func (c *Config) load(path string) (err error) {
	conf := viper.New()
	conf.SetConfigType("yaml")
	conf.SetConfigFile(path)

	err = conf.ReadInConfig()
	if err != nil {
		return
	}

	// tracker configuration
	c.url = conf.GetString("tracker.url")
	c.user = conf.GetString("tracker.user")
	c.password = conf.GetString("tracker.password")
	return
}
