package main

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"mime/multipart"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"os"
	"regexp"
	"strings"

	"golang.org/x/net/publicsuffix"
)

var (
	pattern = `<blockquote><strong>Score:</strong> <span style="color:.*">(-?\d*)</span> \(out of 100\)</blockquote>`
)

type GazelleTracker struct {
	client  *http.Client
	rootURL string
	userID  int
}

func (t *GazelleTracker) Login(user, password string) error {
	form := url.Values{}
	form.Add("username", user)
	form.Add("password", password)
	req, err := http.NewRequest("POST", t.rootURL+"/login.php", strings.NewReader(form.Encode()))
	if err != nil {
		fmt.Println(err.Error())
		return err
	}
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	options := cookiejar.Options{
		PublicSuffixList: publicsuffix.List,
	}
	jar, err := cookiejar.New(&options)
	if err != nil {
		log.Fatal(err)
		return err
	}
	t.client = &http.Client{Jar: jar}
	resp, err := t.client.Do(req)
	if err != nil {
		fmt.Println("Error logging in: " + err.Error())
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return errors.New("Returned status: " + resp.Status)
	}
	if resp.Request.URL.String() == t.rootURL+"/login.php" {
		// if after sending the request we're still redirected to the login page, something went wrong.
		return errors.New("Failed to log in.")
	}
	return nil
}

func (t *GazelleTracker) createRequest(uploadURL string, logPath string) (req *http.Request, err error) {
	// setting up the form
	buffer := new(bytes.Buffer)
	w := multipart.NewWriter(buffer)
	// adding the torrent file
	f, err := os.Open(logPath)
	if err != nil {
		return nil, errors.New("Could not open log: " + err.Error())
	}
	defer f.Close()

	fw, err := w.CreateFormFile("log", logPath)
	if err != nil {
		return nil, errors.New("Could not create form for log: " + err.Error())
	}
	if _, err = io.Copy(fw, f); err != nil {
		return nil, errors.New("Could not read log: " + err.Error())
	}
	w.WriteField("submit", "true")
	w.WriteField("action", "takeupload")
	w.Close()

	req, err = http.NewRequest("POST", uploadURL, buffer)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", w.FormDataContentType())
	return
}

func (t *GazelleTracker) GetLogScore(logPath string) (string, error) {
	if !FileExists(logPath) {
		return "", errors.New("Log does not exist")
	}
	// prepare request
	req, err := t.createRequest(t.rootURL+"/logchecker.php", logPath)
	if err != nil {
		return "", errors.New("Could not prepare upload form: " + err.Error())
	}
	// submit the request
	resp, err := t.client.Do(req)
	if err != nil {
		return "", errors.New("Could not upload log: " + err.Error())
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return "", errors.New("Returned status: " + resp.Status)
	}

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", errors.New("Could not read response")
	}

	// getting log score
	returnData := string(data)
	r := regexp.MustCompile(pattern)
	if r.MatchString(returnData) {
		return r.FindStringSubmatch(returnData)[1], nil
	} else {
		return "", errors.New("Could not find score")
	}
	return "", err
}
