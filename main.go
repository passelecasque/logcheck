package main

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"time"
	"github.com/pkg/errors"
)

func DirectoryExists(path string) (res bool) {
	info, err := os.Stat(path)
	if err != nil {
		return
	}
	if info.IsDir() {
		return true
	}
	return
}

func AbsoluteFileExists(path string) (res bool) {
	info, err := os.Stat(path)
	if err != nil {
		return
	}
	if info.Mode().IsRegular() {
		return true
	}
	return
}

func FileExists(path string) bool {
	var absolutePath string
	if filepath.IsAbs(path) {
		absolutePath = path
	} else {
		currentDir, err := os.Getwd()
		if err != nil {
			return false
		}
		absolutePath = filepath.Join(currentDir, path)
	}
	return AbsoluteFileExists(absolutePath)
}

func FindLogs(dir string) ([]string, error) {
	logs := []string{}
	if ! DirectoryExists(dir) {
		return logs, errors.New("Invalid directory")
	}
	err := filepath.Walk(dir, func(path string, fileInfo os.FileInfo, walkError error) (err error) {
		if fileInfo.Mode().IsRegular() {
			if strings.HasSuffix(path, ".log") {
				logs = append(logs, path)
			}
		}
		return
	})
	return logs, err
}


func main() {
	// load configuration
	conf := &Config{}
	if err := conf.load("config.yaml"); err != nil {
		return
	}
	// log in tracker
	tracker := GazelleTracker{rootURL: conf.url}
	if err := tracker.Login(conf.user, conf.password); err != nil {
		fmt.Println(err.Error())
		return
	}

	// Usage: logcheck (logfile | directory)
	arg := os.Args[1]
	// if arg is a log file, return score
	if strings.HasSuffix(arg, ".log") {
		// upload log and get result
		score, err := tracker.GetLogScore(arg)
		if err != nil {
			fmt.Println(err.Error())
		} else {
			fmt.Println(score + "/100\t"+ arg)
		}
		return
	}
	// else: it's a directory
	// list all logs inside that dir
	fmt.Printf("Searching for logs in %s...\n", arg)
	logs, err := FindLogs(arg)
	if err != nil {
		fmt.Println("ERR: " + err.Error())
		return
	}
	fmt.Printf("++ Found %d logs in %s. ++\n", len(logs), arg)

	// fill channel with all the logs found
	requests := make(chan string, len(logs))
	for _, path := range logs {
		requests <- path
	}
	close(requests)

	// limiter channel: 1 every 2s
	limiter := time.Tick(time.Second * 2)
	perfect := 0
	// process logs one by one, waiting for the tick
	for req := range requests {
		// upload log and get result
		score, err := tracker.GetLogScore(req)
		if err != nil {
			fmt.Println(err.Error())
		} else {
			fmt.Println(score + "/100\t"+ req)
			if score == "100" {
				perfect += 1
			}
		}
		// wait for limiter
		<-limiter
	}
	fmt.Printf("++ Perfect FLACs found: %d/%d, i.e. %d trumpable torrents identified. ++\n", perfect, len(logs), len(logs)-perfect)
}

